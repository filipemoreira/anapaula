<!Doctype HTML>
<html>
    <head>
        <title>[NOME] @yield('title')</title>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="{{ asset( 'style/mestre.css' ) }}">
    </head>
    <body>
        <header>
            <div class="header">
                <a href="" class="buttonLink">
                    <div class="headerButton left">
                        <div class="buttonContainer">
                            Home
                        </div>
                    </div>
                </a>
                <a href="" class="buttonLink">
                    <div class="headerButton left">
                        <div class="buttonContainer">
                            Arquivo
                        </div>
                    </div>
                </a>
                <a href="" class="buttonLink">
                    <div class="headerButton left">
                        <div class="buttonContainer">
                            Contato
                        </div>
                    </div>
                </a>
                <div class="searchBox right">
                    <form action="" method="post">
                        <input type="text" name="search" id="txtSearch">
                        <div class="searchButton right">
                            <img src="{{ asset('images/searchPROVISORIO.png') }}" height=15px" width="15px" />
                            <!-- todo fazer uma lupa melhor-->
                        </div>
                    </form>
                </div>
            </div>
        </header>
        <nav>
            <div class="nav">
                @yield('content')
            </div>
        </nav>
        <footer>
            <div class="footer">
                Design: Anne Marques / Filipe Moreira | Website: Filipe Moreira
            </div>
        </footer>
    </body>
</html>